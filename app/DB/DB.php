<?php  
namespace App\DataBase;

use PDO;

class DB
{
	public $pdo;

	public function __construct()
	{

        $dsn     = "mysql:host={$_ENV['HOST']};dbname={$_ENV['DB']}";
        $options = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'//careful with this one, though
        );

        try {
            $this->pdo = new PDO($dsn, $_ENV['USER'], $_ENV['PASSWORD'], $options);
            //###echo 'Conexión Exitosa ';
            return $this->pdo;
        } catch (PDOException $e) {
            exit('Fallo la conexión: ' . $e->getMessage());
        };

    }

    public function getAllFinales($matricula)
    {
        $sql = "SELECT
                    gusta124_tablas.notasf.`ID Matricula`,
                    gusta124_tablas.`materias, general de`.`Nombre Materia` AS Materia,
                    gusta124_tablas.notasf.`Fecha de Regularidad` AS FR,
                    If((gusta124_tablas.notasf.Parcial1 = 88), 'Equiv.', gusta124_tablas.notasf.Parcial1) AS P1
                FROM
                    gusta124_tablas.notasf
                    INNER JOIN gusta124_tablas.`materias, general de` ON gusta124_tablas.`materias, general de`.`ID materia` =
                    gusta124_tablas.notasf.`ID Materia`
                WHERE
		            gusta124_tablas.notasf.`ID Matricula` = ?";

        $stmt =  $this->pdo->prepare($sql);
        $stmt->bindParam(1, $matricula, PDO::PARAM_INT); // Para evitar SQL injection
        $stmt->execute();
        $tbl = $stmt->fetchAll(); // Obtiene un array PDO::FETCH_ASSOC

        return $tbl;

    }

    public function getAllParciales($matricula)
    {
        $sql = "SELECT
                    gusta124_tablas.notasp.`ID Matricula`,
                    gusta124_tablas.`materias, general de`.`Nombre Materia` AS Materia,
                    gusta124_tablas.notasp.`Fecha de Regularidad` AS FR,
                    gusta124_tablas.notasp.Parcial1 AS P1,
                    gusta124_tablas.notasp.Parcial2 AS P2,
                    gusta124_tablas.notasp.Práctico1 AS TP1,
                    gusta124_tablas.notasp.Práctico2 AS TP2,
                    gusta124_tablas.notasp.Recuperatorio AS R,
                    ELT(gusta124_tablas.notasp.Estado, 'Apr', 'Reg', 'Ins', 'Lib', 'NoAp', 'Aus') AS Est
                FROM
                    gusta124_tablas.notasp
                    INNER JOIN gusta124_tablas.`materias, general de` ON gusta124_tablas.`materias, general de`.`ID materia` =
                    gusta124_tablas.notasp.`ID Materia`
                WHERE
                    gusta124_tablas.notasp.`ID Matricula` = ?";

        $stmt =  $this->pdo->prepare($sql);
        $stmt->bindParam(1, $matricula, PDO::PARAM_INT); // Para evitar SQL injection
        $stmt->execute();
        $tbl = $stmt->fetchAll(); // Obtiene un array PDO::FETCH_ASSOC

        return $tbl;
    }

    /*Return datos de Estado Cuenta*/
    public function getAllEstadoCuenta($matricula)
    {
        $sql = "SELECT p.*, IFNULL(p.Punitorios, 0) AS Punit, rf.id AS feID, rf.Fecha AS feFecha, rf.tipo_cbte, rf.tipo_cbte, CONCAT_WS('-', LPAD(rf.punto_vta, 4,'0'), LPAD(rf.cbte_nro, 8,'0')) AS 'feNro' 
                FROM gusta124_tablas.`alumnos, pagos` p LEFT JOIN gusta124_tablas.`recibos y facturas, tabla de2` rf ON rf.Casa = p.Casa AND rf.Recibo = p.`ID Recibo` 
                WHERE p.`ID Matricula`= ? 
                ORDER BY YEAR(p.Vencimiento), MONTH(p.Vencimiento), p.`Nro de Cuota`, p.Impreso, rf.Fecha;";

        $stmt =  $this->pdo->prepare($sql);
        $stmt->bindParam(1, $matricula, PDO::PARAM_INT); // Para evitar SQL injection
        $stmt->execute();
        $tbl = $stmt->fetchAll(); // Obtiene un array PDO::FETCH_ASSOC

        return $tbl;
    }


}