<?php  
namespace App\Controllers;

/*Llamada a clase bd*/
require 'app/DB/db.php';

/*Mejorar funcionamiento*/
use PDO;
use App\DataBase\DB;

class DefaultController extends AllControllers
{ 
	
	protected $db;
	protected $id;

	public function __construct()
	{
        parent::__construct();

        $this->db = new DB();
		//$this->db = $newBD->pdo;
		$this->id = 25610;
	}

	/*Route:  '/' */
	public function index()
	{
        echo '<h1>Inicio</h1>';

	}

	/*Route:  '/califfinales' */
	public function getFinales()
	{
        $finales = $this->db->getAllFinales($this->id);
        echo $this->twig->render('califfinales.html.twig',
            [
                'finales' => $finales,
                'titulo_txt' => '<span class="trophy-ico"></span> Calificaciones <small>Ex&aacute;menes Finales</small>',
                'titulo_cls' => 'titulo-finalesc',
            ]);
	}

    /*Route:  '/califparciales' */
    public function getCalifPar()
    {
        $parciales = $this->db->getAllParciales($this->id);
        echo $this->twig->render('califparciales.html.twig',
            [
                'parciales' => $parciales,
                'titulo_txt' => '<span class="trophy-ico"></span> Calificaciones <small>Evaluaciones Parciales</small>',
                'titulo_cls' => 'titulo-parcialesc',
            ]);
    }

    /*Route:  '/estadocuenta' */
    public function estadoCuenta($matricula = '25610')
    {
        $estadoCuenta = $this->db->getAllEstadoCuenta($matricula);
        var_dump($estadoCuenta); die;
        echo $this->twig->render('estadoCuenta.html.twig',
            [
                'estadoCuenta' => $estadoCuenta,
            ]);
    }

}
