<?php  
namespace App\Controllers;

/*Clase con funcionalidades para derivar a controladores*/
class AllControllers
{
	public $twig;

	function __construct()
	{
		$loader = new \Twig\Loader\FilesystemLoader('./app/Templates');
		$this->twig = new \Twig\Environment($loader, 
			[
				'debug' => true,
			]);
		$this->twig->addExtension(new \Twig\Extension\DebugExtension());

	}
}
