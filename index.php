<?php
require_once 'vendor/autoload.php';

/*Acceso a Variables de entorno archivo .env*/
$dotenv = Dotenv\Dotenv::createImmutable('./');
$dotenv->load();

$router = new \Bramus\Router\Router();
$router->setNamespace('\App\Controllers');

$router->get('/', 'DefaultController@index');
$router->get('/finales', 'DefaultController@getFinales');
$router->get('/calificaciones', 'DefaultController@getCalifPar');
$router->get('/estadocuenta', 'DefaultController@estadoCuenta');

$router->set404(function() {
    header('HTTP/1.1 404 Not Found');
    echo 'Crear vista para 404';
});


$router->run();

